package com.attinad.rules;

import com.attinad.Box;

import java.util.ArrayList;
import java.util.List;

public class RuleChain {
    private final Box box;
    private List<IRule> ruleList = new ArrayList<>();
    private RuleContext context = new RuleContext();

    public RuleChain(Box box){
        this.box = box;
    }

        public void addRule(IRule rule){

        this.ruleList.add(rule);
    }

    public void triggerRules(){
        for (IRule rule:
             ruleList) {
            rule.fireRule(context,box);
        }
    }
}
