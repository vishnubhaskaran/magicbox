package com.attinad.rules;

import com.attinad.Box;

import java.util.HashMap;

public interface IRule {
    public void fireRule(RuleContext context, Box box);
}
