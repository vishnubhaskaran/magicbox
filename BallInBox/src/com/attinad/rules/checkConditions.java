package com.attinad.rules;

public class checkConditions {
    boolean isFibonacci(int x) {
        return isPerfectSquare(5 * x * x + 4) ||
                isPerfectSquare(5 * x * x - 4);
    }

    boolean isPerfectSquare(int x) {
        int s = (int) Math.sqrt(x);
        return (s * s == x);
    }

    boolean isPrime(int num) {
        int temp;
        boolean isPrime = true;
        for (int i = 2; i <= num / 2; i++) {
            temp = num % i;
            if (temp == 0) {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }
}
