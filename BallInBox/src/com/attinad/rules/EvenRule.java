package com.attinad.rules;

import com.attinad.Box;

import java.util.HashMap;

public class EvenRule implements IRule {

    @Override
    public void fireRule(RuleContext context,Box box) {

        if(context.getValueAsBool(RuleConstants.IS_PRIME))
            return;

        if(box.getTotalBallCount() % 2 == 0){
            box.addBallByColor("BlueBall",box.getCountByColor("BlueBall"));
        }
    }
}
