package com.attinad.rules;

import java.util.HashMap;

public class RuleContext {
    private HashMap<String,Object> contextValues = new HashMap<>();

    public void addValue(String key,Object value){
        this.contextValues.put(key,value);
    }

    public boolean getValueAsBool(String key){
        return Boolean.valueOf(contextValues.get(key).toString());
    }

    public String getValueAsString(String key){
        return contextValues.get(key).toString();
    }

    public Object getValue(String key){
        return contextValues.get(key);
    }
}
