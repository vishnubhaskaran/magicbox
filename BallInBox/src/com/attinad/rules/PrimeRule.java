package com.attinad.rules;

import com.attinad.Box;

import java.util.HashMap;

public class PrimeRule implements IRule {
    @Override
    public void fireRule(RuleContext context,Box box) {
        if(isPrime(box.getTotalBallCount())){
            box.addBallByColor("R",box.getCountByColor("R"));
            box.addBallByColor("B",box.getCountByColor("B")*2);
            box.addBallByColor("G",box.getCountByColor("G")*3);

            context.addValue(RuleConstants.IS_PRIME,true);
        }
     }

    private boolean isPrime(int num) {
        boolean isprime=false;
        int temp;
        for(int i=2;i<num;i++)
        {
             temp=num%i;
             if(temp==0){
                 isprime=false;
                 break;
             }
             isprime=true;
        }

        return isprime;
    }

}
