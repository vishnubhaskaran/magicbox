package com.attinad.balls;

import com.attinad.balls.Ball;
import com.attinad.balls.BlueBall;
import com.attinad.balls.GreenBall;
import com.attinad.balls.RedBall;

public class BallFactory {


    public static Ball createBall(String ch){
        if(ch.equals("R")){
            return new RedBall();
        }
        else if(ch.equals("B")){
            return new BlueBall();
        }
        else if(ch.equals("G")){
            return new GreenBall();
        }
        return  null;
    }
}
