package com.attinad;

import com.attinad.balls.Ball;
import com.attinad.balls.BallFactory;
import com.attinad.rules.*;

import java.util.ArrayList;
import java.util.HashMap;

public class Box {

    private HashMap<String, ArrayList<Ball>> ballMap;

    public Box() {
        ballMap = new HashMap<>();
    }

    public void addBall(String ballColor) {
       // Ball ball = BallFactory.createBall(ballColor);
        this.addBallByColor(ballColor,1);
        performMagic();
    }

    public void performMagic() {
        RuleChain ruleChain = new RuleChain(this);
        ruleChain.addRule(new PrimeRule());
        ruleChain.addRule(new OddRule());
        ruleChain.addRule(new EvenRule());
        ruleChain.addRule(new FibonacciRule());
        ruleChain.triggerRules();
    }

    void display() {
        if (ballMap.containsKey("RedBall")) {
            System.out.println(ballMap.get("RedBall") + "RedBall");
        }
        if (ballMap.containsKey("BlueBall")) {
            System.out.println(ballMap.get("BlueBall") + "BlueBall");
        }
        if (ballMap.containsKey("GreenBall")) {
            System.out.println(ballMap.get("GreenBall") + "GreenBall");
        }
    }


    public int getTotalBallCount() {
        int count = 0;
        for (ArrayList<Ball> ballSet:
                this.ballMap.values()) {
            count += ballSet.size();
        }
        return count;
    }


    public int getCountByColor(String ballColor) {
        return ballMap.get(ballColor).size();
    }

    public void addBallByColor(String ballColor, int ballCount) {
        if(!this.ballMap.containsKey(ballColor)){
            this.ballMap.put(ballColor,new ArrayList<>());
        }
        for (int iCount = 0; iCount < ballCount; iCount++){
            Ball ball = BallFactory.createBall(ballColor);

            this.ballMap.get(ballColor).add(ball);
        }
    }
}
